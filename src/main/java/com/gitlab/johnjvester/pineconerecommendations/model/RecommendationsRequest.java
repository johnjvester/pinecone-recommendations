package com.gitlab.johnjvester.pineconerecommendations.model;

import lombok.Data;

import java.util.List;

@Data
public class RecommendationsRequest {
    private List<SongMetadata> songs;
}
