package com.gitlab.johnjvester.pineconerecommendations.config;

import io.pinecone.PineconeClient;
import io.pinecone.PineconeClientConfig;
import io.pinecone.PineconeConnection;
import io.pinecone.PineconeConnectionConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class PineconeConfiguration {
    private final PineconeConfigurationProperties pineconeConfigurationProperties;

    @Bean
    public PineconeClient pineconeClient() {
        return new PineconeClient(new PineconeClientConfig().withApiKey(pineconeConfigurationProperties.getApiKey()));
    }

    @Bean
    public PineconeConnection pineconeConnection() {
        PineconeClient pineconeClient = new PineconeClient(new PineconeClientConfig().withApiKey(pineconeConfigurationProperties.getApiKey()));

        PineconeConnectionConfig pineconeConnectionConfig = new PineconeConnectionConfig()
                .withServiceAuthority(pineconeConfigurationProperties.getServiceAuthority())
                .withServiceName(pineconeConfigurationProperties.getServiceName());

        return pineconeClient.connect(pineconeConnectionConfig);
    }
}
