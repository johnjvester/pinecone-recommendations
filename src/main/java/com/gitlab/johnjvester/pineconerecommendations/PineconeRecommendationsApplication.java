package com.gitlab.johnjvester.pineconerecommendations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PineconeRecommendationsApplication {
    public static void main(String[] args) {
        SpringApplication.run(PineconeRecommendationsApplication.class, args);
    }
}
